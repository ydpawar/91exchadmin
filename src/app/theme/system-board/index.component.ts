import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DashboardService} from '../../_api';
import {AuthIdentityService, ToastrService} from '../../_services';
import {ActivatedRoute} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

declare var $;
@Component({
  selector: 'app-home',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [DashboardService]
})
export class SystemBoardComponent implements OnInit {

  @ViewChild('indicaters',{static: false}) indicaters: ElementRef;
  @ViewChild('carouselInner',{static: false}) carouselInner: ElementRef;
  dataList: any = [];
  eventList: any = [];
  marketList: any = [];
  marketIdsArr: any = [];
  isActiveTabs: string;
  isEmpty = true;
  systemId: string;
  systemUid: string;
  systemName: string = 'White Label';
  cUserData: any;

  title = 'System Dashboard';
  breadcrumb: any = [];
  // breadcrumb: any = [{title: 'Manage', url: '/' }];

  // tslint:disable-next-line:max-line-length
  constructor(
      private service: DashboardService,
      private route: ActivatedRoute,
      private authIdentity: AuthIdentityService,
      private spinner: NgxSpinnerService,
      private toaster: ToastrService) {
      this.isActiveTabs = 'all';
      this.systemId = this.route.snapshot.params.oid;
      this.bindSliderHTML();
  }

  ngOnInit() {
    this.getDataList(this.isActiveTabs);
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.cUserData = auth.getIdentity();
    }
  }

  async getDataList(type) {
    this.spinner.show();
    const data = {systemId: this.systemId };
    await this.service.getListBySystem(type, data).subscribe(
        // tslint:disable-next-line:no-shadowed-variable
        (data) => {
          this.onSuccessDataList(data);
        },
        error => {
          // this.toaster.error('Error in Get DataList Api !!', 'Something want wrong..!');
        });
  }

  onSuccessDataList(response) {
    if (response.status !== undefined) {
      if (response.status === 1) {
        this.isEmpty = true;
        this.eventList = this.marketList = this.dataList = [];

        if ( response.data.length > 0 ) {
          this.dataList = response.data;
        }

        if ( response.event && response.event.length > 0 ) {
            this.isEmpty = false;
            this.eventList = response.event;
        }

        if ( response.sport.length > 0 ) {
          this.isEmpty = false;
          this.marketList = response.sport;
        }

        if ( response.system ) {
          this.systemName = response.system.systemName;
          this.systemUid = response.system.uid;
        }

      }
    }
    this.spinner.hide();
  }

  bindSliderHTML() {
    const sliderImages = JSON.parse(sessionStorage.getItem('dash-slider'));
    if (sliderImages !== null) {
      let indicaterHTML = '';
      let carouselBodyHTML = '';
      let i = 0;
      const images = [];
      for (const item of sliderImages) {
        images[i] = new Image();
        images[i].src = item.image; // + '?v=' + this.timestamp
        let className = '';
        if (i === 0) {
          className = 'active';
        }
        indicaterHTML += '<li data-target="#DashSlider" data-slide-to="' + i + '" class="' + className + '"></li>';
        carouselBodyHTML += '<div class="carousel-item ' + className + '">';
        carouselBodyHTML += '<img class="d-block img-fluid" loading="lazy" rel="preload" src="' + item.image + '" alt="">';
        carouselBodyHTML += '</div>';
        i++;
      }

      if (this.indicaters && this.carouselInner) {
        this.indicaters.nativeElement.innerHTML = indicaterHTML;
        this.carouselInner.nativeElement.innerHTML = carouselBodyHTML;
        $('#DashSlider').carousel({
          interval: 3000,
          cycle: true
        });
      } else {
        const xhm = setTimeout(() => {
          clearTimeout(xhm);
          this.bindSliderHTML();
        }, 100);
      }
    } else {
      const xhm = setTimeout(() => {
        clearTimeout(xhm);
        this.bindSliderHTML();
      }, 100);
    }
  }

}
