import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ProfitLossComponent } from './index.component';

import {ListComponent} from './list/index.component';
import {ListModule} from './list/index.module';

import {ListEventDetailComponent} from './listEventDetail/index.component';
import {ListEventDetailModule} from './listEventDetail/index.module';

import {ListMarketDetailComponent} from './listMarketDetail/index.component';
import {ListMarketDetailModule} from './listMarketDetail/index.module';

const routes: Routes = [
  {
    path: '',
    component: ProfitLossComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'event-detail',
        component: ListEventDetailComponent
      },
      {
        path: 'market-detail',
        component: ListMarketDetailComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, ListModule, ListEventDetailModule, ListMarketDetailModule
  ], exports: [
    RouterModule
  ], declarations: [ ProfitLossComponent ]
})
export class ProfitLossModule {

}
