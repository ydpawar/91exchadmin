import {AfterViewInit, Component, OnDestroy, OnInit, Injector} from '@angular/core';
import {DashboardService} from '../../_api';
import {BaseComponent} from '../../common/commonComponent';
import swal from "sweetalert2";
declare var $;

@Component({
    selector: 'app-home',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css'],
    providers: [DashboardService]
})

export class SportBlockUnblockComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    title = 'Sport Block Unblock';
    breadcrumb: any = [{title: 'Sport Block Unblock', url: '/'}];
    page = {start: 1, end: 5};
    uid = '';
    isEmpty = false;
    isLoading = false;
    dataTableId = 'DataTables_1';
    aList = [];

    constructor(
        inj: Injector,
        private service: DashboardService,
    ) {
        super(inj);
        this.uid = this.activatedRoute.snapshot.params.uid;
    }

    ngOnInit() {
        this.spinner.show();
        this.applyFilters();
    }

    ngAfterViewInit() {
    }

    ngOnDestroy() {
    }

    applyFilters() {
        this.service.getListByUser(this.uid).subscribe((res) => this.onSuccess(res));
    }

    onSuccess(res) {
        this.spinner.hide();
        if (res.status !== undefined && res.status === 1) {
            $('#DataTables_1').dataTable().fnDestroy();
            if (res.userData !== undefined) {
                this.title = 'Sport Block Unblock for ' + res.userData.name;
            }
            if (res.data !== undefined) {
                if (res.data.length > 0) {
                    this.aList = res.data;
                } else {
                    this.isEmpty = true;
                }
                this.loadScript();
            }
        }
    }

    doSportBlock(sid, status) {
        let statustxt = 'block';
        if (status) {
            statustxt = 'unblock';
        }
        swal.fire({
            title: 'Are you sure to want ' + statustxt + ' ?',
            // text: 'Are you sure to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this.service.doSportBlockByUser(this.uid, sid).subscribe((res) => this.onSuccessSportBlock(res));
            }
        });
    }

    onSuccessSportBlock(res) {
        if (res.status === 1) {
            this.applyFilters();
        }
    }
    loadScript() {
        // tslint:disable-next-line:only-arrow-functions
        $(document).ready(function() {
            $('#DataTables_1').DataTable({
                'scrollX': true,
                'destroy': true,
                'retrieve': true,
                'stateSave': true,
                'language': {
                    'paginate': {
                        'previous': '<i class=\'mdi mdi-chevron-left\'>',
                        'next': '<i class=\'mdi mdi-chevron-right\'>'
                    }
                },
                'drawCallback': function drawCallback() {
                    $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                }
            }); // Complex headers with column visibility Datatable
        });

    }

    getLastChar(id: any) {
        return id.substr(id.length - 6);
    }

}
