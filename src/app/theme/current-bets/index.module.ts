import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CurrentBetsComponent } from './index.component';


const routes: Routes = [
  {
    path: '',
    component: CurrentBetsComponent,
  },
  {
    path: ':uid',
    component: CurrentBetsComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    CurrentBetsComponent
  ]
})
export class CurrentBetsModule {
}
