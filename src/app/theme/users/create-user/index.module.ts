import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CreateUserComponent } from './index.component';

import {CreateComponent} from './create/index.component';
import {CreateModule} from './create/index.module';


const routes: Routes = [
  {
    path: '',
    component: CreateComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule,
    CreateModule
  ], exports: [
    RouterModule
  ], declarations: [ CreateUserComponent]
})
export class CreateUserModule {

}
