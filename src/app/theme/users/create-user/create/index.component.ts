import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ClientService, UsersService } from '../../../../_api/index';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthIdentityService } from '../../../../_services';

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [UsersService]
})
export class CreateComponent implements OnInit, AfterViewInit {

  title = 'Create - User';
  breadcrumb: any = [{ title: 'Create-Users', url: '/' }];

  createOptionList: any;
  userRoleOption: any;

  user: any;
  frm: FormGroup;
  userError: string;
  parentUser: string;
  plError: number;
  tmpplError: number;
  belance: number;
  tmpbelance: number;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UsersService,
    private router: Router,
    private route: ActivatedRoute,
    // tslint:disable-next-line:variable-name
    private _location: Location
  ) {
    const auth = new AuthIdentityService();
    this.userRoleOption = '';

    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.createOptions();
    this.createForm();
    this.checkUserData();
  }

  ngAfterViewInit() {
  }

  createOptions() {
    this.userService.createOptions().subscribe((res) => { this.onSuccessCreateOptions(res); });
  }

  onSuccessCreateOptions(res) {
    if (res.status === 1) {
      this.createOptionList = res.data;
    }
  }

  userRoleOptionChange(e) {
    this.userRoleOption = e;
    this.createForm();
  }

  validatePassword(control: AbstractControl): { [key: string]: any } | null {
    if (control.value) {
      const regex = /\d+/g;
      const onlyLetters = /[a-zA-Z]/;
      if (control.value !== '' || control.value !== undefined || control.value !== null) {
        if (!control.value.match(regex)) {
          return { passwordInvalid: true };
        }
        if (!control.value.match(onlyLetters)) {
          return { passwordInvalid: true };
        }
      }
      return null;
    }
    return null;
  }

  createForm() {
    this.frm = this.formBuilder.group({
      name: ['', [Validators.required,
      Validators.minLength(3),
      Validators.maxLength(20),
      Validators.pattern('[a-zA-Z][a-zA-Z ]+')]],
      username: ['', [Validators.required,
      Validators.minLength(3),
      Validators.maxLength(20)]],
      password: ['', [Validators.required,
      Validators.minLength(6),
      Validators.maxLength(20),
      this.validatePassword
      ]],
      pl: ['0', [Validators.required,
      Validators.max(99),
      Validators.min(0)]],
      balance: ['0', [Validators.required,
      Validators.min(0)]],
      remark: ['']
    });
  }

  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      if( this.userRoleOption == 'Client' ){
        this.userService.createClient(data).subscribe((res) => this.onSuccess(res));
      }
      if(this.userRoleOption == 'Sub Admin'){
        this.userService.createSubAdmin(data).subscribe((res) => this.onSuccess(res));
      }
      if(this.userRoleOption == 'Supervisor'){
        this.userService.createSupervisor(data).subscribe((res) => this.onSuccess(res));
      }
      if(this.userRoleOption == 'Master'){
        this.userService.createMaster(data).subscribe((res) => this.onSuccess(res));
      }
      if(this.userRoleOption == 'Super Master'){
        this.userService.createSuperMaster(data).subscribe((res) => this.onSuccess(res));
      }
      if(this.userRoleOption == 'Super Admin'){
        this.userService.createSuperAdmin(data).subscribe((res) => this.onSuccess(res));
      }
    }

  }

  onSuccess(res) {
    if (res.status === 1) {
      if(this.userRoleOption == 'Client'){
        this.router.navigate(['/users/client']);
      }
      if(this.userRoleOption == 'Sub Admin'){
        this.router.navigate(['/users/sub-admin']);
      }
      if(this.userRoleOption == 'Supervisor'){
        this.router.navigate(['/users/supervisor']);
      }
      if(this.userRoleOption == 'Master'){
        this.router.navigate(['/users/master']);
      }
      if(this.userRoleOption == 'Super Master'){
        this.router.navigate(['/users/super-master']);
      }
      if(this.userRoleOption == 'Super Admin'){
        this.router.navigate(['/users/super-admin']);
      }
    }
  }

  checkUserData() {
    this.userService.checkUserData().subscribe((res) => {
      this.intCheckUser(res);
    });
  }

  intCheckUser(res) {
    if (res.status === 1) {
      this.userError = '';
      // tslint:disable-next-line:triple-equals
      if (res.data != undefined) {
        this.belance = this.tmpbelance = res.data.balance;
        this.plError = this.tmpplError = res.data.pl;
        // this.tmpplError = res.data.pl;
        this.parentUser = res.data.username;
      }
    }
    if (res.status === 0) {
      // tslint:disable-next-line:triple-equals
      if (res.success != undefined) {
        this.userError = res.success.message;
      }
    }
  }

  checkProfitLoss(pl: any) {
    this.plError = this.tmpplError - Number(pl.value);
  }

  checkBalance(balance: any) {
    this.belance = this.tmpbelance - Number(balance.value);
  }

  onCancel() {
    this._location.back();
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  get frmName() { return this.frm.get('name'); }
  get frmUsername() { return this.frm.get('username'); }
  get frmPassword() { return this.frm.get('password'); }
  get frmProfitLoss() { return this.frm.get('pl'); }
  get frmBalance() { return this.frm.get('balance'); }
  get frmRemark() { return this.frm.get('remark'); }

}

