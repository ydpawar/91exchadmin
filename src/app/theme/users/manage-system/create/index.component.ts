import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SuperMasterService } from '../../../../_api/index';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [SuperMasterService]
})
export class CreateComponent implements OnInit, AfterViewInit {

  title = 'Manage system - Create';
  breadcrumb: any = [{title: 'Manage system', url: '/' }, {title: 'Create', url: '/' }];

  frm: FormGroup;
  userError: string;
  parentUser: string;
  plError: number;
  tmpplError: number;
  belance: number;
  tmpbelance: number;

  constructor(
      private formBuilder: FormBuilder,
      private service: SuperMasterService,
      private router: Router,
      private route: ActivatedRoute,
      // tslint:disable-next-line:variable-name
      private _location: Location
  ) {
  }

  ngOnInit() {
    this.createForm();
    this.checkUserData();
  }

  ngAfterViewInit() {
  }

  createForm() {
    this.frm = this.formBuilder.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      pl: ['', Validators.required],
      balance: ['', Validators.required],
      remark: [''],
      systemname: ['', Validators.required],
      systemkey: ['', Validators.required],
      operatorId: ['', Validators.required],
      themeId: ['', Validators.required],
      logo: ['', Validators.required]
    });
  }

  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.frm.reset();
      this.service.createsystem(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this._location.back();
      // this.router.navigate(['/manage']);
    }
  }

  checkUserData() {
    this.service.checkUserData().subscribe((res) => {
      this.intCheckUser(res);
    });
  }

  intCheckUser(res) {
    if (res.status === 1) {
      this.userError = '';
      // tslint:disable-next-line:triple-equals
      if ( res.data != undefined ) {
        this.tmpbelance = res.data.balance;
        this.tmpplError = res.data.pl;
        this.parentUser = res.data.username;
      }
    }
    if (res.status === 0) {
      // tslint:disable-next-line:triple-equals
      if (res.success != undefined) {
        this.userError = res.success.message;
      }
    }
  }

  checkProfitLoss(pl: any) {
    this.plError = this.tmpplError - Number(pl.value);
  }

  checkBalance(balance: any) {
    this.belance = this.tmpbelance - Number(balance.value);
  }

  onCancel() {
    this._location.back();
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  get frmName() { return this.frm.get('name'); }
  get frmUsername() { return this.frm.get('username'); }
  get frmPassword() { return this.frm.get('password'); }
  get frmProfitLoss() { return this.frm.get('pl'); }
  get frmBalance() { return this.frm.get('balance'); }
  get frmRemark() { return this.frm.get('remark'); }
  get frmsystemname() { return this.frm.get('systemname'); }
  get frmsystemkey() { return this.frm.get('systemkey'); }
  get frmsystemId() { return this.frm.get('operatorId'); }
  get frmthemeId() { return this.frm.get('themeId'); }
  get frmlogo() { return this.frm.get('logo'); }

}

