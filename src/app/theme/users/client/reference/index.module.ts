import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ReferenceComponent } from './index.component';

const routes: Routes = [
  {
    path: 'reference/:uid',
    component: ReferenceComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    ReferenceComponent
  ]
})
export class ReferenceModule {

}
