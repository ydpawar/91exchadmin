import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { MarketBetListComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: MarketBetListComponent,
  },
  {
    path: ':mtype/:eid',
    component: MarketBetListComponent,
  },
  {
    path: ':mtype/:eid/:mid',
    component: MarketBetListComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, InfiniteScrollModule
  ], exports: [
    RouterModule
  ], declarations: [
    MarketBetListComponent
  ]
})
export class MarketBetListModule {
}
