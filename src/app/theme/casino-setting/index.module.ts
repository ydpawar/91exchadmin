import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CasinoSettingComponent } from './casino-setting.component';

const routes: Routes = [
  {
    path: '',
    component: CasinoSettingComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, InfiniteScrollModule
  ], exports: [
    RouterModule
  ], declarations: [
    CasinoSettingComponent
  ]
})
export class CasinoSettingModule {
}
