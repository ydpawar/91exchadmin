import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


import { HttpRequestModel, HttpRequestProcessDisplay, RequestOption, HttpService } from './../../_services/index';

@Injectable()
export class CricketService {
  constructor(private http: HttpService) { }

  changestatus(data, status): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'changesportstatus?id=' + data + '&status=' + status;
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  createnewevent(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'createnewevent'; // events/cricket/
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  createnewsportevent(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'createnewsport'; // events/cricket/
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }



  sportListLoad(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'sportlist';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage =  'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }


  cricketeventListLoad(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'cricketeventlist';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage =  'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

}
